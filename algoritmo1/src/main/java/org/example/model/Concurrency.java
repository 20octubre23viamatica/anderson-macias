package org.example.model;

public class Concurrency {

    private int recurrencia;
    private int numero;

    public Concurrency sumar(){
        this.recurrencia++;

        return this;

    }


    public Concurrency(int recurrencia, int numero) {
        this.recurrencia = recurrencia;
        this.numero = numero;
    }

    public void setRecurrencia(int recurrencia) {
        this.recurrencia = recurrencia;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getRecurrencia() {
        return recurrencia;
    }

    public int getNumero() {
        return numero;
    }

    @Override
    public String toString() {
        return "Concurrency{" +
                "recurrencia=" + recurrencia +
                ", numero=" + numero +
                '}';
    }
}