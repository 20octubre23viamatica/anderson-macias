package org.example.util;

import org.example.model.Concurrency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ArrayUtils {

    public static Concurrency mayorConcurrencia(int[] array) {

        ArrayList<Concurrency> concurrencies = new ArrayList<>();

        for (int number : array) {

            if (concurrencies.isEmpty()) {
                concurrencies.add(new Concurrency(1, number));
                continue;
            }

            int indice = -1;

            for (int j = 0; concurrencies.size() > j; j++) {

                indice = concurrencies.get(j).getNumero() == number ? j : -1;

                if (indice < 0)
                    concurrencies.add(new Concurrency(1, number));
                else
                    concurrencies.get(indice).sumar();

            }

        }

        Collections.sort(concurrencies, Comparator.comparingInt(Concurrency::getRecurrencia).reversed());

        return concurrencies.get(0);

    }

}